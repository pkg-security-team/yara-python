#!/bin/sh

set -e

VERSION=$2
FILE=$3

NEWVERSION=${VERSION}+dfsg
NEWFILE=../yara-python_${NEWVERSION}.orig.tar.xz

echo "Generating ${NEWFILE} ..."
zcat $FILE \
    | tar --wildcards --delete -f - '*/yara/*' \
    | xz -c > ${NEWFILE}.t

mv ${NEWFILE}.t ${NEWFILE}
